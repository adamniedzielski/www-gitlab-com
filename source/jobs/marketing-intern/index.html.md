---
layout: job_page
title: "Marketing Intern"
---

## Responsibilities
* Work with Field Marketing to own the Diversity Scholarship program Support Field Marketing Manager in all upcoming events and conferences.
* Work with Content Marketing to support the content strategy via blog posts, social media posts, etc.
* Act as internal PR coordinator between our PR firm + GitLab broader marketing team.
